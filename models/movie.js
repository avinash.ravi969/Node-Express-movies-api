const mongoose = require('mongoose');
const Joi = require('joi');

const { genreSchema } = require('./genre');

const Movie = mongoose.model('Movie', new mongoose.Schema({
    title: {
        type: String,
        required: true,
        min: 5,
        max: 50,
        trim: true
    },
    numberInStock: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    },
    genre: {
        type: genreSchema,
        required: true
    }
}));

function validateMovie(movie) {
    const schema = {
        title: Joi.string.min(5).max(50).required(),
        numberInStock: Joi.Number.min(0).max(255).required(),
        dailyRentalRate: Joi.Number.min(0).max(255).required(),
        genreId: Joi.string.required()
    };
    return Joi.validate(movie, schema);
}

exports.Movie = Movie;
exports.validate = validateMovie;