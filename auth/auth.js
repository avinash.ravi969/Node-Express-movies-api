const Joi = require('joi');
const bcrypt = require('bcrypt');
const config = require('config');
const express = require('express');
const router = express.Router();

const { User } = require('../models/user');

router.post('/', async (req, res) => {
    const { error } = validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);
     
    let user = await User.findOne({ email: req.body.email });
    if(!user) res.send('Invalid User or password');
  
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) res.send('Invalid User or password');

    const token = user.generateAuthToken();
    res.send(token);
});

function validate(req) {
    const schema = {
        password: Joi.string().min(5).max(1024).required(),
        email: Joi.string().min(5).max(255).required().email()
    };

    return Joi.validate(req, schema);
}

module.exports = router;