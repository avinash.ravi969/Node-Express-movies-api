const express = require('express');
const router = express.Router();
const Fawn = require('fawn');

Fawn.init(mongoose);

const { Rental, validate } = require('../models/rental');
const { Customer } = require('../models/customer');
const { Movie } = require('../models/movie');

router.get('/', async (req, res) => {
    const rentals = await Router.find().sort('-dateOut');
    res.send(rentals);
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    if(!mongoose.Types.ObjectId.isValid(req.body.customerId)) return res.status(400).send('Invalid Customer..');
    if(!mongoose.Types.ObjectId.isValid(req.body.movieId)) return res.status(400).send('Invalid Movie..');

    const customer = await Customer.findById(req.body.customerId);
    if(!customer) return res.status(400).send('Invalid Customer....');

    const movie = await Movie.findById(req.body.movieId);
    if(!movie) return res.status(400).send('Invalid Movie....');

    if(movie.numberInStock === 0) return res.status(400).send('Movie Not InStock....');

    let rental = new rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            numberInStock: movie.numberInStock,
            dailyRentalRate: movie.dailyRentalRate
        }
    });

    try {
        new Fawn.Task()
        .save('rentals', rental)
        .update('movies', { _id: movie._id }, { 
            $inc: { numberInStock: -1 }
        })
        .run();

        res.send(rental);
    } catch(error) {
        res.status(500).send('Internal Server Error..');
    }
    
});

module.exports = router;